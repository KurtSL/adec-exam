$(function() {

    const mq = window.matchMedia( "(min-width: 992px)" );

    function checkMedia(){
        if (mq.matches) {
        // window width is at least 992px
            $('.sidebar').mCustomScrollbar("destroy");
            $('.advanced-search').mCustomScrollbar({
                theme: "minimal-dark"
            });
        } else {
        // window width is less than 992px
            $('.sidebar').mCustomScrollbar({
                theme: "minimal-dark"
            });
            $('.advanced-search').mCustomScrollbar("destroy")
        }
    }
    checkMedia();

    $(window).on('resize', function(){
        checkMedia();
    });

    $('.details-box').mCustomScrollbar({
        theme: "minimal-dark"
    });


    $('#sidebarCollapse').on('click', function () {
        // Open/Close Sidebar
        $('.sidebar, .content').toggleClass('sidebar-collapsed');
        // Close collapsible 
        $('.sidebar .components .collapsible').collapse('hide');
        
    });

    $('#detailsBoxCollapse').on('click', function () {
        // Open/Close Sidebar
        $('.details-box, .content').toggleClass('details-box-collapsed');
        // Close collapsible 
        
    });

    $('[data-toggle="tooltip"]').tooltip('show');
    

});